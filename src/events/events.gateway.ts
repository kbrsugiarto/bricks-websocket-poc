import {
  WebSocketGateway,
  WebSocketServer,
  SubscribeMessage,
  MessageBody,
  WsResponse,
} from '@nestjs/websockets';
import { Server } from 'socket.io';
// import { Server } from 'ws';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';

interface User {
  id: number;
  name: string;
}

const users = [
  { id: 1, name: 'John' },
  { id: 2, name: 'Doe' },
];

@WebSocketGateway()
export class EventsGateway {
  @WebSocketServer()
  server: Server;

  @SubscribeMessage('login')
  login(@MessageBody() userId: number): Observable<WsResponse<User>> {
    return from(users).pipe(
      map(user => ({
        event: 'user',
        data: user,
      })),
    );
  }

  @SubscribeMessage('getUser')
  getUserById(@MessageBody() userId: number): User {
    return users.find(user => user.id === userId);
  }

  @SubscribeMessage('events')
  findAll(@MessageBody() data: string): Observable<WsResponse<number>> {
    return from([1, 2, 3]).pipe(map(item => ({ event: 'events', data: item })));
  }

  @SubscribeMessage('identity')
  async identity(@MessageBody() data: number): Promise<number> {
    return data;
  }
}
